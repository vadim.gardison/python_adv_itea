from time import time, sleep


class DecorTimeCrit:
    def __init__(self, critical_time):
        self.critical_time = critical_time
        
    @staticmethod
    def timer(func, critical_time):
        def helper(*args, **kwargs):
            time_start = time()
            res = func(*args, **kwargs)
            method_time = time() -time_start
            if method_time > critical_time:
                print(f'WARN!!! {func.__name__} slow. Time = {time() - time_start} sec')
            return res
        return helper

    def __call__(self, cls):
        def helper(*args, **kwargs):
            for attr in dir(cls):
                if attr.startswith('__'):
                    continue
                    
                new_attr = getattr(cls, attr)

                if callable(new_attr):
                    decor_attr = DecorTimeCrit.timer(new_attr, self.critical_time)
                    setattr(cls, attr, decor_attr)

            return cls(*args, **kwargs)

        return helper


@DecorTimeCrit(critical_time=0.45)
class Test:
    def method_1(self):
        print('slow method start')
        sleep(1)
        print('slow method finish')

    def method_2(self):
        print('fast method start')
        sleep(0.1)
        print('fast method finish')


t = Test()

t.method_1()
t.method_2()


# slow method start
# slow method finish
# WARNING! method_1 slow. Time = ??? sec.
# fast method start
# fast method finish
