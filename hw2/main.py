from Library import Library
from Book import Book
from Reader import Reader

library = Library()

### get all books ###
# for book in library.get_all_books():
#     print(book["title"])

### get books in lib (in stock) ###
# for book in library.get_books_inlib():
#     print(book["title"])

### get books not in lib (not in stock) ###
# for book in library.get_books_not_inlib():
#     print(book["title"])

### create book ###
# book = Book('Python_adv', 'Mike Mike', 2000)

### create reader ###
# reader = Reader('John', 30)

### add book ###
# print(library.add_book(book.get_book()))

### del book ###
# print(library.del_book(237537460128627230564400221610990441637))

### add reader ###
# print(library.add_reader(reader.get_reader()))

### transfer book to reader ###
# print(library.book_to_reader(108713448394089935085146844264780527815, 1))

### transfer book to lib from reader ###
print(library.book_to_lib(108713448394089935085146844264780527815, 84652330665373692377611361752744227908))