import uuid

class Reader:
    """Reader class
    """

    def __init__(self, name: str, birth_day: int, book_id: list = []):
        self.__id = uuid.uuid4()
        self.__name = name
        self.__birth_day = birth_day
        self.__book_id = book_id

    def get_reader(self) -> dict:
        """returns dictionary of object fields

        Returns:
            dict: object fields
        """

        return dict(
            id = int(self.__id), # ??? тут была ошибка, поэтому привел к инту... TypeError: Object of type UUID is not JSON serializable ???
            name = self.__name,
            birth_day = self.__birth_day,
            book_id = self.__book_id
        )