import task_1
import task_2

while True:
    
    try:
        program = int(input('Enter number of task (1 or 2): '))
        if program == 1:
            print('You chose task_1')
            task_1.main()
            break
        elif program == 2:
            print('You chose task_2')
            task_2.main()
            break
    except ValueError:
        print('Must be DIGIT!!!')