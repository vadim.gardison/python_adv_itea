# 2. Дана матрица целых чисел 10x10. Вводится число. Выяснить, какие строки и
# столбцы матрицы содержат данное число. (либо рандом либо чтение из файла (input.txt))

def main():

    number = input('Enter number: ')
    matrix = list()
    rows = list()
    columns = list()
    
    with open("hw1/input.txt", "r") as file:
        for line in file:
            matrix.append(line.split())

    for r in matrix:
        for c in r:
            if c == number:
                rows.append(str(matrix.index(r)))
                columns.append(str(r.index(c)))

    if len(rows) == 0:
        print('No results!')
    else:
        print(f'Rows (index): {" ".join(rows)}')
        print(f'Columns (index): {" ".join(columns)}')


if __name__ == '__main__':
    main()
