### task_1 ###
# 1. Программа принимает от пользователя диапазоны для коэффициентов a, b, c квадратного уравнения: ax2 + bx + c = 0.
# Перебирает все варианты целочисленных коэффициентов в указанных диапазонах, определяет квадратные уравнения,
# которые имею решение

from math import sqrt


def quadratic(a, b, c):

    disc = b ** 2 - 4 * a * c

    if disc < 0:
        return f'{a}, {b}, {c} No'
    elif disc == 0:
        return f'{a}, {b}, {c} Yes {-(b / 2 * a)}'
    else:
        sqrt_disc = sqrt(disc)
        x1 = (-b - sqrt_disc) / (2 * a)
        x2 = (-b + sqrt_disc) / (2 * a)
        return f'{a}, {b}, {c}, {disc} Yes {round(x1, 2)} {round(x2, 2)}'


def compare(a, b):
    if a > b:
        raise Warning('Compare warning!')


def main():
    try:
        a1 = int(input('a1: '))
        a2 = int(input('a2: '))
        compare(a1, a2)

        b1 = int(input('b1: '))
        b2 = int(input('b2: '))
        compare(b1, b2)

        c1 = int(input('c1: '))
        c2 = int(input('c2: '))
        compare(c1, c2)

    except ValueError:
        print('Enter digits only !!! Try again...')

    except Warning:
        print('Wrong range. First coef must be less than second! Try again')

    else:
        for i in range(a1, a2 + 1):
            if i == 0:
                continue
            for j in range(b1, b2 + 1):
                for k in range(c1, c2 + 1):
                    print(quadratic(i, j, k))


if __name__ == "__main__":
    main()
