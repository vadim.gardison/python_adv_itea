import json
import os


class Library:
    """Library class
    """

    def __init__(self):

        with open(f"{os.getcwd()}/python_adv_itea/hw6/server/books.json", "r") as books:
            self.__books_list = json.load(books)

        with open(f"{os.getcwd()}/python_adv_itea/hw6/server/readers.json", "r") as readers:
            self.__readers_list = json.load(readers)

    def get_all_books(self) -> list:
        """get all books method

        Returns:
            list: returns list of book_dicts
        """

        return self.__books_list

    def get_books_inlib(self) -> list:
        """get books in stock

        Returns:
            list: list of books in library
        """

        return list(filter(lambda el: el["reader_id"] == None, self.get_all_books()))
    
    def get_books_not_inlib(self) -> list:
        """get books not in stock

        Returns:
            list: list of books not in library
        """

        return list(filter(lambda el: el["reader_id"] != None, self.get_all_books()))

    def get_all_readers(self) -> list:
        """get all readers

        Returns:
            list: 
        """

        return self.__readers_list

    def __update_library_books(self, file: str, entity: list):
        with open(f"hw2/{file}.json", "w") as elem:
            json.dump(entity, elem)
    
    def add_book(self, book: dict) -> list:
        """add book to library method

        Args:
            book (dict): book entity (dictionary)

        Returns:
            list: new list of books
        """

        books_list = self.get_all_books()
        books_list.append(book)
        self.__update_library_books('books', books_list)
        # with open("hw2/books.json", "w") as books:
        #     json.dump(books_list, books)

        return books_list

    def del_book(self, id: int) -> list:
        """delete book from library method

        Args:
            id (int): id of book to delete

        Returns:
            list: new list of books
        """

        new_books_list = list(
            filter(lambda el: el["id"] != id, self.get_all_books()))

        with open("hw2/books.json", "w") as books:
            json.dump(new_books_list, books)

        return new_books_list

    def add_reader(self, reader: dict) -> list:
        """add reader to library method

        Args:
            reader (dict): reader entity (dictionary)

        Returns:
            list: new list of readers
        """

        readers_list = self.get_all_readers()
        readers_list.append(reader)
        self.__update_library_books('readers', readers_list)
        # with open("hw2/readers.json", "w") as readers:
        #     json.dump(readers_list, readers)

        return readers_list

    def book_to_reader(self, book_id: int, reader_id: int) -> list:
        """transfer book to reader

        Args:
            book_id (int): 
            reader_id (int): 

        Returns:
            list: new books list
        """

        all_books = self.get_books_inlib()
        all_readers = self.get_all_readers()

        if book_id in list(map(lambda el: el["id"], all_books)):
            if reader_id in list(map(lambda el: el["id"], all_readers)):

                def book_to_reader_books_mapper(el):
                    """map callback/ add reader_id to book entity
                    """
                    if el["id"] == book_id:
                        el["reader_id"] = reader_id
                        return el
                    else:
                        return el

                def book_to_reader_readers_mapper(el):
                    """map callback/ add book_ids to reader
                    """
                    if el["id"] == reader_id:
                        el["book_id"].append(book_id)
                        return el
                    else:
                        return el

                new_all_books = list(
                    map(book_to_reader_books_mapper, all_books))
                new_all_readers = list(
                    map(book_to_reader_readers_mapper, all_readers))

                self.__update_library_books('books', new_all_books)
                # with open("hw2/books.json", "w") as books:
                #     json.dump(new_all_books, books)

                self.__update_library_books('readers', new_all_readers)
                # with open("hw2/readers.json", "w") as readers:
                #     json.dump(new_all_readers, readers)

                # return new_all_books
                result = dict(books = new_all_books, readers = new_all_readers)
                return result
            else:
                return "There is no suсh reader....Add him"
        else:
            return "There is no such book in lib"

    def book_to_lib(self, book_id: int, reader_id: int) -> list:
        """transfer book to lib from reader

        Args:
            book_id (int):
            reader_id (int):

        Returns:
            list: new books list
        """

        all_books = self.get_books_not_inlib()
        all_readers = self.get_all_readers()

        if book_id in list(map(lambda el: el["id"], all_books)):
            if reader_id in list(map(lambda el: el["id"], all_readers)):

                def book_to_lib_books_mapper(el):
                    """map callback/ remove reader_id from book entity
                    """
                    if el["id"] == book_id:
                        el["reader_id"] = None
                        return el
                    else:
                        return el

                def book_to_lib_readers_mapper(el):
                    """map callback/ remove book_ids from reader
                    """
                    if el["id"] == reader_id:
                        updated_books_list = list(
                            filter(lambda elem: book_id not in el["book_id"], el["book_id"]))
                        el["book_id"] = updated_books_list
                        return el
                    else:
                        return el

                new_all_books = list(
                    map(book_to_lib_books_mapper, all_books))
                new_all_readers = list(
                    map(book_to_lib_readers_mapper, all_readers))

                self.__update_library_books('books', new_all_books)
                # with open("hw2/books.json", "w") as books:
                #     json.dump(new_all_books, books)

                self.__update_library_books('readers', new_all_readers)
                # with open("hw2/readers.json", "w") as readers:
                #     json.dump(new_all_readers, readers)

                # return new_all_books
                return dict(books = new_all_books, readers = new_all_readers)

            else:
                return "There is no suсh reader in lib"
        else:
            return "Wrong book"
