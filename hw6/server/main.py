import socket
import pickle
import os
from msgutils import recv_msg, send_msg
from Library import Library

library = Library()
with socket.socket() as sock:
    sock.bind(('localhost', 12345))

    while True:
        sock.listen(2)
        conn, _ = sock.accept()

        data = recv_msg(conn)

        parsed_data = pickle.loads(data)

        if parsed_data["action"] == '1':
            print(os.path.dirname(os.path.abspath(__file__)))
            all_books = library.get_all_books()
            print(f'selected action {parsed_data["action"]}')
            print(all_books)

        elif parsed_data["action"] == '2':
            print(f'selected action {parsed_data["action"]}')

        # conn.send(data.upper())
        send_msg(conn, data)
