import socket
from msgutils import recv_msg, send_msg
import pickle

# with socket.socket() as sock:
#     sock.connect(('localhost', 12345))
    # time.sleep(7)

user_msg = '''Enter action:
1) Get all books;
2) Get books in Lib
3) Get books not in Lib
5) Create book
6) Create reader
7) Add book to Lib
8) Del book from Lib
9) Add reader to Lib
10) Transfer book to reader
11) Transfer book to lib from reader
0) Exit'''

def request(req_param: str) -> dict:
    with socket.socket() as sock:
        sock.connect(('localhost', 12345))
        # time.sleep(7)

        # sock.send(user_query.encode(encoding='utf-8'))

        send_msg(sock, req_param)

        # data = sock.recv(1024)
        data = recv_msg(sock)

        # return f'\n{data.decode(encoding="utf-8")}\n'
        return data

while True:
    try:

        print(user_msg)
        
        user_query = input()

        if user_query == '0':
            break

        if user_query in [str(i) for i in range(12)]:

            if user_query == '1':
                response_data = request(pickle.dumps({"action": user_query}))
                print(response_data)
               
            elif user_query == '2':
                response_data = request(pickle.dumps({"action": user_query, "data": "test1"}))
                print(response_data)

            elif user_query == '3':
                response_data = request(pickle.dumps({"action": user_query, "data": "test2"}))
                print(response_data)

        else:
            print('Choose right action!')
    
    except ValueError:
        print('Error')