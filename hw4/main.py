class Range:
    def __init__(self, iter_num):
        self.iter_num = iter_num
        self.count = -1

    def __iter__(self):
        return self

    def __next__(self):
        self.count += 1

        if self.count < self.iter_num:
            return self.count
        else:
            raise StopIteration

o = Range(5)

print(next(o))
print(next(o))
print(next(o))

    