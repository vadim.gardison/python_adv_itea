from setuptools import setup

setup(
    name='hw5_library_package',
    version='1.0.0',
    description='Lib for homework 5',
    author='VG',
    author_email='vadim.gardison@gmail.com',
    packages=['hw5_library_package'],
)
