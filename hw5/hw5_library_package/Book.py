import uuid


class Book:
    """Book class
    """

    def __init__(self, title: str, author: str, publ_year: int, reader_id: list = []):
        self.__id = uuid.uuid4()
        self.__title = title
        self.__author = author
        self.__publ_year = publ_year
        self.__reader_id = reader_id

    def get_book(self) -> dict:
        """returns dictionary of object fields

        Returns:
            dict: object fields
        """

        return dict(
            id = int(self.__id), # ??? тут была ошибка, поэтому привел к инту... TypeError: Object of type UUID is not JSON serializable ???
            title = self.__title,
            author = self.__author,
            publ_year = self.__publ_year,
            reader_id = self.__reader_id
        )
